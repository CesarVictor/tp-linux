# TP2 : Appréhender l'environnement Linux
## I. Service SSH
### 1. Analyse du service
**🌞 S'assurer que le service sshd est démarré**
```
[user1@localhost ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-12-05 11:32:21 CET; 2min 9s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 718 (sshd)
      Tasks: 1 (limit: 5905)
     Memory: 5.6M
        CPU: 56ms
     CGroup: /system.slice/sshd.service
             └─718 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Dec 05 11:32:21 localhost systemd[1]: Starting OpenSSH server daemon...
Dec 05 11:32:21 localhost.localdomain sshd[718]: Server listening on 0.0.0.0 port 22.
Dec 05 11:32:21 localhost.localdomain sshd[718]: Server listening on :: port 22.
Dec 05 11:32:21 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Dec 05 11:32:59 localhost.localdomain sshd[864]: Accepted password for user1 from 192.168.222.5 port 56586 ssh2
Dec 05 11:32:59 localhost.localdomain sshd[864]: pam_unix(sshd:session): session opened for user user1(uid=1000) by (uid=0)
```
**🌞 Analyser les processus liés au service SSH**
```
[user1@localhost ~]$ ps -ef | grep sshd
root         718       1  0 11:32 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root         864     718  0 11:32 ?        00:00:00 sshd: user1 [priv]
user1        870     864  0 11:32 ?        00:00:00 sshd: user1@pts/0
user1       1069     871  0 11:39 pts/0    00:00:00 grep --color=auto sshd
```
**🌞 Déterminer le port sur lequel écoute le service SSH**
```
[user1@localhost ~]$ ss | grep ssh
tcp   ESTAB  0      52                   192.168.222.4:ssh       192.168.222.5:56586
```
> C'est du tcp donc on liste tous les ports tcp en écoute :
```
[user1@localhost ~]$ ss -alnpt
State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port            Process
LISTEN            0                 128                                0.0.0.0:22                                0.0.0.0:*
LISTEN            0                 128                                   [::]:22                                   [::]:*
```
> On sait que le service ssh écoute sur le port 22

**🌞 Consulter les logs du service SSH**
```
[user1@localhost ~]$ journalctl -xe | grep sshd
Dec 05 11:32:20 localhost systemd[1]: Created slice Slice /system/sshd-keygen.
░░ Subject: A start job for unit sshd-keygen@ecdsa.service has finished successfully
░░ A start job for unit sshd-keygen@ecdsa.service has finished successfully.
░░ Subject: A start job for unit sshd-keygen@ed25519.service has finished successfully
░░ A start job for unit sshd-keygen@ed25519.service has finished successfully.
░░ Subject: A start job for unit sshd-keygen@rsa.service has finished successfully
░░ A start job for unit sshd-keygen@rsa.service has finished successfully.
Dec 05 11:32:20 localhost systemd[1]: Reached target sshd-keygen.target.
░░ Subject: A start job for unit sshd-keygen.target has finished successfully
░░ A start job for unit sshd-keygen.target has finished successfully.
░░ Subject: A start job for unit sshd.service has begun execution
░░ A start job for unit sshd.service has begun execution.
Dec 05 11:32:21 localhost.localdomain sshd[718]: Server listening on 0.0.0.0 port 22.
Dec 05 11:32:21 localhost.localdomain sshd[718]: Server listening on :: port 22.
░░ Subject: A start job for unit sshd.service has finished successfully
░░ A start job for unit sshd.service has finished successfully.
Dec 05 11:32:59 localhost.localdomain sshd[864]: Accepted password for user1 from 192.168.222.5 port 56586 ssh2
Dec 05 11:32:59 localhost.localdomain sshd[864]: pam_unix(sshd:session): session opened for user user1(uid=1000) by (uid=0)
Dec 05 11:34:30 localhost.localdomain sudo[1059]:    user1 : TTY=pts/0 ; PWD=/home/user1 ; USER=root ; COMMAND=/bin/systemctl status sshd
Dec 05 11:55:00 localhost.localdomain sudo[1123]:    user1 : TTY=pts/0 ; PWD=/etc/ssh ; USER=root ; COMMAND=/bin/cat sshd_config
```
```
[user1@localhost ~]$ sudo tail -n 10 /var/log/secure
Dec  5 11:49:33 localhost sudo[1097]: pam_unix(sudo:session): session closed for user root
Dec  5 11:49:47 localhost sudo[1101]:   user1 : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/nano lastlog
Dec  5 11:49:47 localhost sudo[1101]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1000)
Dec  5 11:49:50 localhost sudo[1101]: pam_unix(sudo:session): session closed for user root
Dec  5 11:50:11 localhost sudo[1109]:   user1 : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat maillog
Dec  5 11:50:11 localhost sudo[1109]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1000)
Dec  5 11:50:11 localhost sudo[1109]: pam_unix(sudo:session): session closed for user root
Dec  5 11:50:55 localhost sudo[1113]:   user1 : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure
Dec  5 11:50:55 localhost sudo[1113]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1000)
Dec  5 11:50:55 localhost sudo[1113]: pam_unix(sudo:session): session closed for user root
```
### 2. Modification du service

**🌞 Identifier le fichier de configuration du serveur SSH**

> Dans ``/etc/ssh`` , c'est le fichier _`sshd_config `_

**🌞 Modifier le fichier de conf**

```
[user1@localhost ~]$ echo $RANDOM
8123
```
```
[user1@localhost ~]$ sudo cat /etc/ssh/sshd_config | grep Port
 Port 8123
```
```
user1@localhost ~]$ sudo firewall-cmd --remove-port=22/tcp --permanent
[sudo] password for user1:
Warning: NOT_ENABLED: 22:tcp
success
```
```
[user1@localhost ~]$ sudo firewall-cmd --add-port=8123/tcp --permanent
success

```
```
[user1@localhost ~]$ sudo firewall-cmd --reload
success
```
```
[user1@localhost ~]$ sudo firewall-cmd --list-all | grep 8123
  ports: 8123/tcp
```
**🌞 Redémarrer le service**
```
sudo systemctl restart sshd
```
**🌞 Effectuer une connexion SSH sur le nouveau port**
```
C:\Users\cesar>ssh user1@192.168.222.4
ssh: connect to host 192.168.222.4 port 22: Connection refused

C:\Users\cesar>ssh user1@192.168.222.4 -p 8123
user1@192.168.222.4's password:
Last login: Tue Dec  6 10:31:40 2022 from 192.168.222.5
[user1@localhost ~]$
```

## II. Service HTTP
### 1. Mise en place
**🌞 Installer le serveur NGINX**
```
[user1@localhost ~]$ sudo dnf install nginx
```
**🌞 Démarrer le service NGINX**
```
[user1@localhost ~]$ sudo systemctl enable nginx
```
```
[user1@localhost ~]$ sudo systemctl start nginx
```
```
[user1@localhost ~]$ sudo firewall-cmd --permanent --add-service=http
```
```
[user1@localhost ~]$ sudo firewall-cmd --permanent --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client http ssh
  ports: 22/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
```
[user1@localhost ~]$ sudo firewall-cmd --reload
success
```
```
[user1@localhost ~]$ sudo systemctl status nginx | grep active
     Active: active (running) since Tue 2022-12-06 11:13:21 CET; 6min ago
```
**🌞 Déterminer sur quel port tourne NGINX**
```
[user1@localhost ~]$ sudo cat /etc/nginx/nginx.conf | grep listen
        listen       80;
        listen       [::]:80;
```
> Il écoute sur le port 80

```
[user1@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
```
[user1@localhost ~]$ sudo firewall-cmd --reload
success
```
```
[user1@localhost ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 22/tcp 80/tcp
```
**🌞 Déterminer les processus liés à l'exécution de NGINX**
```
[user1@localhost ~]$ ps -ef | grep nginx
root        9168       1  0 11:13 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       9173    9168  0 11:13 ?        00:00:00 nginx: worker process
user1      11013    1077  0 11:32 pts/1    00:00:00 grep --color=auto nginx
```
**🌞 Euh wait**
```
[user1@localhost ~]$ curl http://192.168.222.4:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   372k      0 --:--:-- --:--:-- --:--:--  372k
```
### 2. Analyser la conf de NGINX
**🌞 Déterminer le path du fichier de configuration de NGINX**
```
[user1@localhost ~]$ ls -al /etc/nginx | grep conf
drwxr-xr-x   2 root root    6 Oct 31 16:37 conf.d
-rw-r--r--   1 root root 1077 Oct 31 16:37 fastcgi.conf
-rw-r--r--   1 root root 1077 Oct 31 16:37 fastcgi.conf.default
-rw-r--r--   1 root root 2334 Oct 31 16:37 nginx.conf
-rw-r--r--   1 root root 2656 Oct 31 16:37 nginx.conf.default
```
>PATH : __/etc/nginx/nginx.conf__

**🌞 Trouver dans le fichier de conf**
```
[user1@localhost ~]$ sudo cat /etc/nginx/nginx.conf | grep server -A 10
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }

```
```
[user1@localhost ~]$ sudo cat /etc/nginx/nginx.conf | grep include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;
```
### 3. Déployer un nouveau site web
**🌞 Créer un site web**
```
[user1@localhost ~]$ cat /var/www/tp2_linux/index.html
<h1>MEOW mon premier serveur web</h1>
```
**🌞 Adapter la conf NGINX**
```
[user1@localhost conf.d]$ sudo cat tp2.conf
server {
  listen 18375;

  root /var/www/tp2_linux;
}
```
**🌞 Visitez votre super site web**
```
[user1@localhost default.d]$ curl http://192.168.222.4:18375/
<h1>MEOW mon premier serveur web</h1>
```
## III. Your own services

### 2. Analyse des services existants

**🌞 Afficher le fichier de service SSH**

```
[user1@localhost default.d]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

**🌞 Afficher le fichier de service NGINX**

```
[user1@localhost default.d]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
```

### 3. Création de service

**🌞 Créez le fichier /etc/systemd/system/tp2_nc.service**
```
[user1@localhost system]$ cat tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 8634
```

**🌞 Indiquer au système qu'on a modifié les fichiers de service**

```
[user1@localhost system]$ sudo systemctl daemon-reload
```

**🌞 Démarrer notre service de ouf**

```
[user1@localhost system]$ sudo systemctl start tp2_nc.service
```

**🌞 Vérifier que ça fonctionne**

```
[user1@localhost system]$ sudo systemctl status tp2_nc.service
[sudo] password for user1:
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Sun 2022-12-18 19:33:00 CET; 36min ago
   Main PID: 1028 (nc)
      Tasks: 1 (limit: 5905)
     Memory: 1.1M
        CPU: 1ms
     CGroup: /system.slice/tp2_nc.service
             └─1028 /usr/bin/nc -l 8634

Dec 18 19:33:00 localhost.localdomain systemd[1]: Started Super netcat tout fou.
```
```
[user1@localhost system]$ sudo ss -tulpan | grep nc
tcp   LISTEN 0      10                  0.0.0.0:8634        0.0.0.0:*     users:(("nc",pid=1028,fd=4))
tcp   LISTEN 0      10                     [::]:8634           [::]:*     users:(("nc",pid=1028,fd=3))
```

1er Vm:
```
[user1@localhost system]$ sudo systemctl status tp2_nc.service
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Sun 2022-12-18 19:33:00 CET; 48min ago
   Main PID: 1028 (nc)
      Tasks: 1 (limit: 5905)
     Memory: 1.1M
        CPU: 1ms
     CGroup: /system.slice/tp2_nc.service
             └─1028 /usr/bin/nc -l 8634

Dec 18 19:33:00 localhost.localdomain systemd[1]: Started Super netcat tout fou.
Dec 18 20:16:33 localhost.localdomain nc[1028]: hello
Dec 18 20:17:31 localhost.localdomain nc[1028]: world
```
2eme Vm:
```
[user1@localhost ~]$ nc 192.168.222.4 8634
hello
world
```

**🌞 Les logs de votre service**

```
[user1@localhost system]$ sudo journalctl -xe -u tp2_nc | grep systemd
Dec 18 19:33:00 localhost.localdomain systemd[1]: Started Super netcat tout fou.
```
```
[user1@localhost system]$ sudo journalctl -xe -u tp2_nc -f | grep hello
Dec 18 20:16:33 localhost.localdomain nc[1028]: hello
```
```
 sudo journalctl -xe -u tp2_nc | grep systemd
Dec 18 20:34:53 localhost.localdomain systemd[1]: tp2_nc.service: Deactivated successfully.
```

**🌞 Affiner la définition du service**
```
[user1@localhost system]$ cat tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 8634
Restart=always
```
```
sudo systemctl daemon-reload
```
