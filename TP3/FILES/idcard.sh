#!/bin/bash

echo "Machine name : $(hostnamectl hostname)"
echo "OS : $(cat /etc/redhat-release | cut -d' ' -f1-2) and kernel version is $(uname -v)"
echo "IP : $(ip a | grep 192.168.222.255 | cut -d' ' -f8)"
echo "RAM :$( free -h | grep Mem | tr -s ' ' | cut -d' ' -f4) memory available on $( free -h | grep Mem | tr -s ' ' | cut -d' ' -f2) total memory"
echo "Disk : $(df --si | grep /dev/mapper/rl-root | tr -s ' ' | cut -d' ' -f4) space left"
echo "Top 5 processes by RAM usage :"
ram_output="$(ps aux | sort -rnk 4 | head -5 | tr -s ' ' | cut -d' ' -f11)"
while read victor; do
    echo "  - ${victor}"
done <<< "$ram_output"
echo "Listening ports :"
ss_output="$(ss -altpnu4H)"
while read cesar; do
    port_type="$(echo ${cesar} | cut -d' ' -f1)"
    port_number="$(echo ${cesar} | cut -d' ' -f5 | cut -d':' -f2)"
    program="$(echo ${cesar} | cut -d' ' -f7 |  cut -d'(' -f3 | cut -d',' -f1 | cut -d'"' -f2)"
    echo "  - $port_type $port_number $program"
done <<< "$ss_output"
curl https://cataas.com/cat -o cat.png
echo "Here is your random cat : cat.png"
