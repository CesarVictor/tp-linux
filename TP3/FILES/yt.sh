#!bin/bash

URL=$1

if [[ ! -d "/srv/yt/downloads" ]]
then
    echo "Fail loooser"
    exit
fi
TITLE_VIDEO=$(/usr/local/bin/youtube-dl -e "$URL")
DIR_PATH="/srv/yt/downloads/$TITLE_VIDEO"
FILE_PATH="$DIR_PATH/$TITLE_VIDEO.mp4"
mkdir "$DIR_PATH" &> /dev/null
/usr/local/bin/youtube-dl "$URL" --output="$FILE_PATH" &> /dev/null
if [[ ! -d "/var/log/yt/" ]]
then
    echo "Fail looser"
    exit
fi
youtube-dl --get-description "$URL" > "$DIR_PATH/description" 2> /dev/null
echo "Video $URL was downloaded."
echo "File path : $FILE_PATH"
echo "[$(date +%D) $(date +%T)] Video $URL was downloaded. File path : $FILE_PATH" >> /var/log/yt/download.log
