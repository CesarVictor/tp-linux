#!bin/bash
FILE_URL=/srv/yt/yt_url
while true; do
video_url=$(head -n1 "${FILE_URL}")
if [[ -z "${video_url}" ]] ; then
	sleep 10
else
    if [[ ! -d "/srv/yt/downloads" ]] ; then
        echo "Fail loooser"
        exit
    fi
    TITLE_VIDEO=$(/usr/local/bin/youtube-dl -e "${video_url}")
    DIR_PATH="/srv/yt/downloads/$TITLE_VIDEO"
    FILE_PATH="$DIR_PATH/$TITLE_VIDEO.mp4"
    mkdir "$DIR_PATH" &> /dev/null
    /usr/local/bin/youtube-dl "${video_url}" --output="$FILE_PATH" &> /dev/null
    if [[ ! -d "/var/log/yt/" ]] ; then
        echo "Fail looser"
        exit
    fi
    /usr/local/bin/youtube-dl --get-description "${video_url}" > "$DIR_PATH/description" 2> /dev/null
    echo "Video $URL was downloaded."
    echo "File path : $FILE_PATH"
    echo "[$(date "+%D %T")] Video $video_url was downloaded. File path : $FILE_PATH" >> /var/log/yt/download.log
    chmod o+w /srv/yt/yt_url
    sed -i '1d' "${FILE_URL}"
fi
done
