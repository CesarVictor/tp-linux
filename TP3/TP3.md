# TP3 : We do a little scripting
## Script carte d'identité

[idcard.sh](./FILES/idcard.sh)
```
[user1@localhost idcard]$ ./idcard.sh
Machine name : localhost
OS : Rocky Linux and kernel version is #1 SMP PREEMPT Tue Sep 20 17:53:31 UTC 2022
IP : 192.168.222.255
RAM :630Mi memory available on 960Mi total memory
Disk : 2.8G space left
Top 5 processes by RAM usage :
  - /usr/bin/python3
  - /usr/sbin/NetworkManager
  - /usr/lib/systemd/systemd
  - /usr/lib/systemd/systemd
  - /usr/lib/systemd/systemd-logind
Listening ports :
  - udp 323 chronyd
  - tcp 22 sshd
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 63474  100 63474    0     0    98k      0 --:--:-- --:--:-- --:--:--   98k
Here is your random cat : cat.png
```

## Script youtube-dl

[yt.sh](./FILES/yt.sh)

```
[user1@localhost yt]$ bash yt.sh https://www.youtube.com/watch?v=sNx57atloH8
Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded.
File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
```
[fichier de log](./FILES/download.log)
```
[user1@localhost idcard]$ cat /var/log/yt/download.log
[12/13/22 11:10:48] Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded. File path : /srv/yt/downloads//.mp4
[12/13/22 11:17:07] Video https://www.youtube.com/watch?v=__1SjDrSMik was downloaded. File path : /srv/yt/downloads//.mp4
```

### MAKE IT SERVICE

[yt-v2.sh](./FILES/yt-v2.sh)

[Fichier service](./FILES/yt.service)

```
[user1@localhost system]$ sudo systemctl status yt
● yt.service - Service pour télécharger des vidéos youtube
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: disabled)
     Active: active (running) since Sun 2022-12-18 18:51:51 CET; 52s ago
   Main PID: 2143 (yt-v2.sh)
      Tasks: 2 (limit: 5905)
     Memory: 568.0K
        CPU: 12ms
     CGroup: /system.slice/yt.service
             ├─2143 bin/bash /srv/yt/yt-v2.sh
             └─2155 sleep 10

Dec 18 18:51:51 localhost.localdomain systemd[1]: Started Service pour télécharger des vidéos youtube.
```

```
[user1@localhost system]$ sudo !!
sudo journalctl -xe -u yt

Dec 18 18:51:51 localhost.localdomain systemd[1]: Started Service pour télécharger des vidéos youtube.
░░ Subject: A start job for unit yt.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit yt.service has finished successfully.
░░
░░ The job identifier is 1760
```