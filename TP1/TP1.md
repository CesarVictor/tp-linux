# TP1 : Are you dead yet?
## I.Intro
-
## II.Feu

### 1er Façon :
`Réecrire le fichier systeme`

On va dans le dossier `boot`
```
$ cd /boot
```
On crée un fichier Adieu.txt avec un petit message d'adieu
```
$ nano Adieu.txt
```
Puis en envoie ce fichier réecrire le fichier système
```
$ echo Adieu.txt >  symvers-5.14.0-70.26.1.e19_0.x86_64
```
### 2eme façon :
On va dans le dossier `etc`

```
cd /etc
```
On edite le fichier `bashrc` qui permet d'avoir une interface utilisateur
```
$ nano bashrc
```
on enlève la condition du premier if :
````
if []; then
  BASHRCSOURCED="Y"
````
Bye Bye 2eme VM

### 3eme façon : On modifie un fichier dans le dossier loader de boot

On se rend dans le dossier qui nous intéresse
```
$ cd /boot/loader/entries
```
On liste les fichiers 
```
# ls
c09f731fedb046c8bccc65a6be4a6e41-0-rescue.conf                     c09f731fedb046c8bccc65a6be4a6e41-5.14.0-70.26.1.el9_0.x86_64.conf
c09f731fedb046c8bccc65a6be4a6e41-5.14.0-70.13.1.el9_0.x86_64.conf
```
On ouvre le fichier avec la version de notre vm
```
nano *26*
```
```
title Rocky Linux (5.14.0-70.26.1.el9_0.x86_64) 9.0 (Blue Onyx)
version 5.14.0-70.26.1.el9_0.x86_64
linux /vmlinuz-5.14.0-70.26.1.el9_0.x86_64
initrd /initramfs-5.14.0-70.26.1.el9_0.x86_64.img
options root=/dev/mapper/rl-root ro crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M resume=/dev/mapper/rl-swap rd.lvm.lv=rl/root rd.lvm.lv=rl/swap
grub_users $grub_users
grub_arg --unrestricted
grub_class rocky
```
Puis on supprime juste `root=`

Bye bye 3eme VM

### 4eme méthode :`Reebot la machine à chaque log`

On modifie le fichier profile qui va être lu dès le lancement de la vm, il permet de se connecter
````
vim /etc/profile
````
On écrit `reboot` au début du fichier 
```
# /etc/profile

# System wide environment and startup programs, for login setup
# Functions and aliases go in /etc/bashrc

# It's NOT a good idea to change this file unless you know what you
# are doing. It's much better to create a custom.sh shell script in
# /etc/profile.d/ to make custom changes to your environment, as this
# will prevent the need for merging in future updates.
reboot

pathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}
```
Voilà à chaque fois qu'un utilisateur va essayer de se connecter la vm va redémarrer automatiquement.


